package ir.ppaz.bors;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 2020/02/08
 * Mohammad Mohammadi
 */
public class App {
    public static void main(String[] args) {
        String symbol = "اخابر‍";
        String fromDate = "1380/01/01";
        String toDate = "1398/11/16";

        try {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        public void checkClientTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }

                        public void checkServerTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }

        String financialPeriodApiUrl = "https://search.codal.ir/api/search/v1/financialYears?Symbol=" + symbol;
        List<String> financialPeriods = new ArrayList<>();
        try {
            InputStream is = new URL(financialPeriodApiUrl).openStream();

            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);

            financialPeriods = Arrays.asList(jsonText.substring(1).substring(0, jsonText.length() - 2).replaceAll("\"", "").split(","));
            System.out.println(financialPeriods);
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
        String companiesListApiUrl = "https://search.codal.ir/api/search/v1/companies";
        try {
            InputStream is = new URL(companiesListApiUrl).openStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject jsonObject = new JSONObject("{\"companies\":" + jsonText + "}");
            JSONArray companies = jsonObject.getJSONArray("companies");
            for (int i = 0; i < companies.length(); i++) {
                JSONObject company = companies.getJSONObject(i);
                System.out.println(company);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
        try {
            int totalPage = 1;
            for (int page = 1; page <= totalPage; page++) {
                String lettersListApiUrl = "https://search.codal.ir/api/search/v2/q?Audited=true&AuditorRef=-1&Category=1&Childs=false&CompanyState=-1&CompanyType=-1&Consolidatable=true&FromDate=" + fromDate + "&IsNotAudited=false&Length=12&LetterType=-1&Mains=false&NotAudited=false&NotConsolidatable=true&PageNumber=" + page + "&Publisher=false&Symbol=" + symbol + "&ToDate=" + toDate + "&TracingNo=-1&search=true";
                InputStream is = new URL(lettersListApiUrl).openStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
                String jsonText = readAll(rd);
                JSONObject jsonObject = new JSONObject(jsonText);
                totalPage = jsonObject.getInt("Page");
                JSONArray letters = jsonObject.getJSONArray("Letters");
                for (int i = 0; i < letters.length(); i++) {
                    JSONObject letter = letters.getJSONObject(i);
                    System.out.println(letter);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

}
